package com.tts.restaurant.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Cart(
    val cuisineID: Int,
    val cuisine: String,
    val desc: String,
    val price: Double,
    val quantity: Int
) : Parcelable {
}