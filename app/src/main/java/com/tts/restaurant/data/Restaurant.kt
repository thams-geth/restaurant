package com.tts.restaurant.data

data class Restaurant(val name: String, val icon: Int, val rating: Double, val cuisine: String) {
}