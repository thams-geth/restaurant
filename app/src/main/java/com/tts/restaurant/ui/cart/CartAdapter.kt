package com.tts.restaurant.ui.cart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tts.restaurant.data.Cart
import com.tts.restaurant.databinding.ItemCartBinding
import com.tts.restaurant.ui.details.InterfaceOnItemClick

class CartAdapter(var cart: ArrayList<Cart>, var onClick: InterfaceOnItemClick, var showmore: Boolean) :
    RecyclerView.Adapter<CartAdapter.ViewHolder>() {

    private var cartList = ArrayList<Cart>()
    var inteface: InterfaceOnItemClick = onClick


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun getItemCount(): Int {
        return cart.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        cartList = cart as ArrayList<Cart>
        holder.bind(cart[position], cartList, inteface, showmore, position)

//        if (showmore) {
//            holder.bind(cart[position], cartList, inteface, showmore)
//        } else {
//            if (position < 2) {
//                holder.bind(cart[position], cartList, inteface, showmore)
//            }
//        }
    }

    class ViewHolder private constructor(private val binding: ItemCartBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: Cart,
            cartList: ArrayList<Cart>,
            clickListener: InterfaceOnItemClick,
            showmore: Boolean,
            position: Int
        ) {
            if (!showmore && position > 1) {
                binding.view.visibility = View.GONE
            } else {
                binding.view.visibility = View.VISIBLE

            }
            binding.tvCuisineName.text = item.cuisine
            binding.tvDesc.text = item.desc
            binding.tvPrice.text = "€ ${item.price}"
            if (item.quantity != 0) {
                binding.tvAdd.visibility = View.GONE
                binding.llAddandMinus.visibility = View.VISIBLE
                binding.tvQuantity.text = "" + item.quantity
            } else {
                binding.tvAdd.visibility = View.VISIBLE
                binding.llAddandMinus.visibility = View.GONE
            }

            binding.tvAdd.setOnClickListener {
                binding.tvAdd.visibility = View.GONE
                binding.llAddandMinus.visibility = View.VISIBLE
                //setting item count
                binding.tvQuantity.text = "" + (binding.tvQuantity.text.toString().toInt() + 1)
                cartList.add(Cart(item.cuisineID, item.cuisine, item.desc, item.price, binding.tvQuantity.text.toString().toInt()))
                clickListener.onItemClick(cartList)

            }
            binding.ivMinus.setOnClickListener {
                val cartItem =
                    cartList.lastIndexOf(
                        Cart(
                            item.cuisineID,
                            item.cuisine,
                            item.desc,
                            item.price,
                            binding.tvQuantity.text.toString().toInt()
                        )
                    )

                binding.tvQuantity.text = "" + (binding.tvQuantity.text.toString().toInt() - 1)
                if (binding.tvQuantity.text.toString().toInt() == 0) {
                    binding.tvAdd.visibility = View.VISIBLE
                    binding.llAddandMinus.visibility = View.GONE
                    cartList.removeAt(cartItem)
                } else {
                    cartList[cartItem] = Cart(item.cuisineID, item.cuisine, item.desc, item.price, binding.tvQuantity.text.toString().toInt())
                }
                clickListener.onItemClick(cartList)


            }
            binding.ivPlus.setOnClickListener {

                val cartItem =
                    cartList.lastIndexOf(
                        Cart(
                            item.cuisineID,
                            item.cuisine,
                            item.desc,
                            item.price,
                            binding.tvQuantity.text.toString().toInt()
                        )
                    )

                if (binding.tvQuantity.text.toString().toInt() != 10) {
                    binding.tvQuantity.text = "" + (binding.tvQuantity.text.toString().toInt() + 1)
                    cartList[cartItem] = Cart(item.cuisineID, item.cuisine, item.desc, item.price, binding.tvQuantity.text.toString().toInt())
                }

                clickListener.onItemClick(cartList)

            }

        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding =
                    ItemCartBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

}

class CartListener(val clickListener: (cartList: ArrayList<Cart>) -> Unit) {
    fun onClick(cartList: ArrayList<Cart>) = clickListener(cartList)
}