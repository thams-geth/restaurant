package com.tts.restaurant.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.tts.restaurant.R
import com.tts.restaurant.data.Restaurant
import com.tts.restaurant.databinding.FragmentHomeBinding

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentHomeBinding>(
            inflater,
            R.layout.fragment_home, container, false
        )
//        val restaurantViewModel = ViewModelProvider(this).get(RestaurantViewModel::class.java)
//        binding.restaurantViewModel = restaurantViewModel

        val restaurant = mutableListOf<Restaurant>(
            Restaurant("Faasos", R.drawable.ic_bell_covering_hot_dish, 4.3, "Wraps, North Indian, Biryani, Fast Food"),
            Restaurant("McDonald's", R.drawable.ic_cheeseburger, 4.1, "Balewadi High Street, Baner"),
            Restaurant("Inka Restaurant", R.drawable.ic_french_fries, 4.8, "Quac de la Costa"),
            Restaurant("Sukkubhai Biriyani", R.drawable.ic_pizza, 4.8, "Biryani, North Indian, Mughlai, Desserts, Beverages"),
            Restaurant("Hotel Santosi", R.drawable.ic_soda_drink, 4.8, "Chinese, Chettinad, North Indian"),
            Restaurant("Coal Barbecues", R.drawable.ic_star_border_black_24dp, 4.8, "North Indian, Chinese")
        )

        val adapter = RestaurantAdapter(restaurant, RestaurantListener { name ->
            Toast.makeText(context, name, Toast.LENGTH_SHORT).show()
            this.findNavController().navigate(
                (R.id.action_homeFragment_to_restaurantDetailsFragment)
            )
        })
        binding.lifecycleOwner = this

        binding.recyclerView.adapter = adapter



        return binding.root
    }


}
