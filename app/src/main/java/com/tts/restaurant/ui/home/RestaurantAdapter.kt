package com.tts.restaurant.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tts.restaurant.data.Restaurant
import com.tts.restaurant.databinding.ItemRestaurantBinding

class RestaurantAdapter(var restaurant: List<Restaurant>, private val clickListener: RestaurantListener) :
    RecyclerView.Adapter<RestaurantAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun getItemCount(): Int {
        return restaurant.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(restaurant[position], clickListener)
    }

    class ViewHolder private constructor(private val binding: ItemRestaurantBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Restaurant, clickListener: RestaurantListener) {
            binding.restaurant = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
//            binding.tvResName.text = item.name
//            binding.tvCuisine.text = item.cuisine
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding =
                    ItemRestaurantBinding.inflate(layoutInflater, parent, false)
//                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_restaurant, parent, false)
                return ViewHolder(binding)
            }
        }
    }

}

class RestaurantListener(val clickListener: (name: String) -> Unit) {
    fun onClick(restaurant: Restaurant) = clickListener(restaurant.name)
}