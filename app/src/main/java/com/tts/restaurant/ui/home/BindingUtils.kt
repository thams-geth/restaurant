package com.tts.restaurant.ui.home

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.tts.restaurant.data.Restaurant
import kotlinx.android.synthetic.main.item_restaurant.view.*


@BindingAdapter("setRestaurantName")
fun TextView.setRestaurantName(item: Restaurant) {
    text = item.name
}

@BindingAdapter("setCuisineName")
fun TextView.setCuisineName(item: Restaurant) {
    text = item.cuisine
}

@BindingAdapter("setIcon")
fun ImageView.setIcon(item: Restaurant) {
    Glide.with(this).load(item.icon).fitCenter().into(imageView)
}

