package com.tts.restaurant.ui.details

import com.tts.restaurant.data.Cart

interface InterfaceOnItemClick {
    fun onItemClick(cartList: ArrayList<Cart>)
}