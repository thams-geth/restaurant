package com.tts.restaurant.ui.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tts.restaurant.data.Cart
import com.tts.restaurant.databinding.ItemCartBinding

class CuisineAdapter(
    var cuisine: List<Cart>,
    var onClick: InterfaceOnItemClick,
    cartList: ArrayList<Cart>
) :
    RecyclerView.Adapter<CuisineAdapter.ViewHolder>() {

    private var cartList = cartList
    var inteface: InterfaceOnItemClick = onClick


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun getItemCount(): Int {
        return cuisine.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(cuisine[position], cartList, inteface)
    }

    class ViewHolder private constructor(private val binding: ItemCartBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: Cart,
            cartList: ArrayList<Cart>,
            clickListener: InterfaceOnItemClick
        ) {
            binding.tvCuisineName.text = item.cuisine
            binding.tvDesc.text = item.desc
            binding.tvPrice.text = "€ ${item.price.toString()}"

            if (item.quantity != 0) {
                binding.tvAdd.visibility = View.GONE
                binding.llAddandMinus.visibility = View.VISIBLE
                binding.tvQuantity.text = "" + item.quantity
            } else {
                binding.tvAdd.visibility = View.VISIBLE
                binding.llAddandMinus.visibility = View.GONE
            }

            binding.tvAdd.setOnClickListener {
                binding.tvAdd.visibility = View.GONE
                binding.llAddandMinus.visibility = View.VISIBLE
                //setting item count
                binding.tvQuantity.text = "" + (binding.tvQuantity.text.toString().toInt() + 1)
                cartList.add(Cart(item.cuisineID, item.cuisine, item.desc, item.price, binding.tvQuantity.text.toString().toInt()))
                clickListener.onItemClick(cartList)


            }
            binding.ivMinus.setOnClickListener {
                val cartItem =
                    cartList.lastIndexOf(
                        Cart(
                            item.cuisineID,
                            item.cuisine,
                            item.desc,
                            item.price,
                            binding.tvQuantity.text.toString().toInt()
                        )
                    )

                binding.tvQuantity.text = "" + (binding.tvQuantity.text.toString().toInt() - 1)
                if (binding.tvQuantity.text.toString().toInt() == 0) {
                    binding.tvAdd.visibility = View.VISIBLE
                    binding.llAddandMinus.visibility = View.GONE
                    cartList.removeAt(cartItem)
                } else {
                    cartList[cartItem] = Cart(item.cuisineID, item.cuisine, item.desc, item.price, binding.tvQuantity.text.toString().toInt())
                }
                clickListener.onItemClick(cartList)


            }
            binding.ivPlus.setOnClickListener {

                val cartItem =
                    cartList.lastIndexOf(
                        Cart(
                            item.cuisineID,
                            item.cuisine,
                            item.desc,
                            item.price,
                            binding.tvQuantity.text.toString().toInt()
                        )
                    )

                if (binding.tvQuantity.text.toString().toInt() != 10) {
                    binding.tvQuantity.text = "" + (binding.tvQuantity.text.toString().toInt() + 1)
                    cartList[cartItem] = Cart(item.cuisineID, item.cuisine, item.desc, item.price, binding.tvQuantity.text.toString().toInt())
                }

                clickListener.onItemClick(cartList)

            }

        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding =
                    ItemCartBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

}
