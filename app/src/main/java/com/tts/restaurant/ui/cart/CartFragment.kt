package com.tts.restaurant.ui.cart

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.tts.restaurant.R
import com.tts.restaurant.data.Cart
import com.tts.restaurant.databinding.FragmentCartBinding
import com.tts.restaurant.ui.details.InterfaceOnItemClick


/**
 * A simple [Fragment] subclass.
 */
class CartFragment : Fragment(), InterfaceOnItemClick {

    lateinit var binding: FragmentCartBinding
    lateinit var adapter: CartAdapter
    private var showmore: Boolean = false


    private var cartList = ArrayList<Cart>()
    private val newList = arrayListOf<Cart>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentCartBinding>(
            inflater,
            R.layout.fragment_cart, container, false
        )

        binding.toolbar.title = "My Cart"
        binding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        binding.toolbar.setNavigationOnClickListener(View.OnClickListener { activity!!.onBackPressed() })

        binding.btnEmpty.setOnClickListener {
            this.findNavController().navigate(
                (R.id.action_cartFragment_to_restaurantDetailsFragment)
            )
        }
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            findNavController().navigate(CartFragmentDirections.actionCartFragmentToRestaurantDetailsFragment(cartList = cartList.toTypedArray()))
        }


        val list = CartFragmentArgs.fromBundle(arguments!!).cartList
        cartList = list.toMutableList() as ArrayList<Cart>


        adapter = CartAdapter(cartList, this, showmore)
        binding.recyclerView.adapter = adapter

        var totalcost: Double = 0.0
        for (i in cartList) {
            totalcost += (i.price * i.quantity)
        }
        binding.tvTotalCost.text = "$ $totalcost"


//
//        if (cartList.size > 2) {
//            for (x in 0..1) {
//                newList.add(cartList[x])
//            }
//            adapter = CartAdapter(newList, this)
//            binding.recyclerView.adapter = adapter
//        } else {
//            adapter = CartAdapter(cartList, this)
//            binding.recyclerView.adapter = adapter
//            binding.tvShowMore.visibility = View.GONE
//        }

        if (cartList.size > 2) {
            binding.tvShowMore.visibility = View.VISIBLE
        }
        binding.tvShowMore.setOnClickListener {
            showmore = true
            adapter = CartAdapter(cartList, this, showmore)
            binding.recyclerView.adapter = adapter

//            for (x in 2 until cartList.size) {
//                newList.add(cartList[x])
//            }
            adapter.notifyDataSetChanged()
            binding.tvShowMore.visibility = View.INVISIBLE
        }

        binding.clCart.setOnClickListener {
            Toast.makeText(context, "Order Placed", Toast.LENGTH_SHORT).show()
            this.findNavController().navigate(
                (R.id.action_cartFragment_to_restaurantDetailsFragment)
            )
        }
        return binding.root
    }

    override fun onItemClick(list: ArrayList<Cart>) {
        Log.d(TAG, list.toString())

        cartList = list

        adapter = CartAdapter(cartList, this, showmore)
        binding.recyclerView.adapter = adapter

        adapter.notifyDataSetChanged()


        if (cartList.size <= 0) {
            binding.clCart.visibility = View.GONE
            binding.tvTotalCost.text = "$ 0"
            binding.btnEmpty.visibility = View.VISIBLE

        } else {
            binding.clCart.visibility = View.VISIBLE
            binding.btnEmpty.visibility = View.GONE

            var totalcost: Double = 0.0
            for (i in cartList) {
                totalcost += (i.price * i.quantity)
            }
            binding.tvTotalCost.text = "$ $totalcost"
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home ->
                findNavController().navigate(CartFragmentDirections.actionCartFragmentToRestaurantDetailsFragment(cartList = cartList.toTypedArray()))
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TAG = "CartFragment"
    }
}
