package com.tts.restaurant.ui.details

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.tts.restaurant.R
import com.tts.restaurant.data.Cart
import com.tts.restaurant.databinding.FragmentRestaurantDetailsBinding
import com.tts.restaurant.ui.cart.CartFragmentArgs

/**
 * A simple [Fragment] subclass.
 */
class RestaurantDetailsFragment : Fragment(), InterfaceOnItemClick {
    lateinit var binding: FragmentRestaurantDetailsBinding
    lateinit var cartList: ArrayList<Cart>
    private var cart = ArrayList<Cart>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        binding = DataBindingUtil.inflate<FragmentRestaurantDetailsBinding>(
            inflater,
            R.layout.fragment_restaurant_details, container, false
        )
        setHasOptionsMenu(true)
        binding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        binding.toolbar.setNavigationOnClickListener(View.OnClickListener { activity!!.onBackPressed() })
        binding.toolbar.inflateMenu(R.menu.restaurant_menu)
        binding.toolbar.setOnMenuItemClickListener {

            when (it.itemId) {
                R.id.menuShare -> shareSuccess()
            }
            return@setOnMenuItemClickListener true

        }

        binding.clCart.visibility = View.GONE

        val cuisine = mutableListOf<Cart>(
            Cart(12, "Chicken Combo Meal ", "Egg+Fish Fry+Pepper Chicken+Chicken 65+Fish Kuzhambu+Mutton Kuzhambu", 244.0, 0),
            Cart(13, "Boneless Chicken 65", "Served with tomato sauce, chilli sauce", 140.0, 0),
            Cart(15, "Chicken Biryani", "Biryani cooked in fire wood", 178.0, 0),
            Cart(20, "Prawns Friedrice", "with tomato sauce", 184.0, 0),
            Cart(22, "Prawns Friedrice", "with tomato sauce", 184.0, 0),
            Cart(40, "Prawns Friedrice", "with tomato sauce", 184.0, 0),
            Cart(55, "Prawns Friedrice", "with tomato sauce", 184.0, 0),
            Cart(70, "Paneer Manchurian Dry", "Served with chilli sauce", 134.0, 0)

        )

        cartList = ArrayList()

//        var cartList: ArrayList<Cart>? = null
        arguments?.let {
            if (it.size() > 0) {
                val list = CartFragmentArgs.fromBundle(it).cartList
                cartList = list.toMutableList() as ArrayList<Cart>
                for (i in list) {
                    var position: Int = 0
                    for (j in cuisine) {
                        if (i.cuisineID == j.cuisineID) {
                            cuisine[position] = Cart(j.cuisineID, j.cuisine, j.desc, j.price, i.quantity)
                        }
                        position++
                    }
                }
                setCartItem(cartList)
            }

        }


        val adapter = CuisineAdapter(cuisine, this, cartList)
        binding.recyclerView.adapter = adapter
        binding.clCart.setOnClickListener {
            this.findNavController()
                .navigate(RestaurantDetailsFragmentDirections.actionRestaurantDetailsFragmentToCartFragment(cartList = cart.toTypedArray()))
        }
        return binding.root
    }

    override fun onItemClick(cartList: ArrayList<Cart>) {
        Log.d(TAG, cartList.toString())
        setCartItem(cartList)

    }


    private fun setCartItem(cartList: ArrayList<Cart>) {

        cart = cartList
        if (cartList.size <= 0) {
            binding.clCart.visibility = View.GONE
        } else {
            binding.clCart.visibility = View.VISIBLE
            var quantity = 0
            for (i in cartList) {
                quantity += i.quantity
            }
            binding.tvCartList.text = "VIEW CART($quantity items)"
        }
    }

    // Showing the Share Menu Item Dynamically
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.restaurant_menu, menu)
        // check if the activity resolves
        if (null == getShareIntent().resolveActivity(activity!!.packageManager)) {
            // hide the menu item if it doesn't resolve
            menu.findItem(R.id.menuShare)?.setVisible(false)
        }
    }

    // Sharing from the Menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuShare -> shareSuccess()
        }
        return super.onOptionsItemSelected(item)

    }

    private fun shareSuccess() {
        startActivity(getShareIntent())
    }

    // Creating our Share Intent
    private fun getShareIntent(): Intent {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain")
            .putExtra(Intent.EXTRA_TEXT, "Hey, Guys take a look at this wonderful restaurant app")
        return shareIntent
    }

    companion object {
        private const val TAG = "RestaurantDetails"
    }
}
